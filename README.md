# st

This is my build of [st](https://st.suckless.org).

## Patches

* [st-xresources](https://st.suckless.org/patches/xresources/st-xresources-20200604-9ba7ecf.diff)
* [st-boxdraw](https://st.suckless.org/patches/boxdraw/st-boxdraw_v2-0.8.5.diff)
* [st-ligatures-boxdraw](st-ligatures-boxdraw-20240427-0.9.2.diff
)
* [st-glyph-wide-support-boxdraw](st-glyph-wide-support-boxdraw-20220411-ef05519.diff), but I had to manually merge this with the ligatures patch
